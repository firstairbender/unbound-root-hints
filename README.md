# Unbound root hints service

Update root hints for unbound.

Inspired from: https://wiki.archlinux.org/index.php/unbound#Roothints_systemd_timer

See: https://wiki.archlinux.org/index.php/unbound#Root_hints

## Editing the timer
If you want to change the frequency of the timer, or any aspect of the service, see [this](https://wiki.archlinux.org/index.php/systemd#Editing_provided_units
)
